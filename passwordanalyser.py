import string
import random
password = input("Please Enter a password:")
length = len(password)

""" 
Critera for a strong password

Note:
This is very generic and WIP, i shamelessly stole the rules from here: https://www.muhlenberg.edu/offices/oit/about/policies_procedures/strong-passwords.html
Score system also WIP, can be changed later

- Must be at least 12 charachters Long
- Must include 1 uppercase letter
- Must include 1 lowercase letter
- Must include 1 special charachter
- Must include 1 number

  You get 1 point for each rule, max points are 5

- Scoring System:

  1 point: Weak password
  2, 3 and 4 points: Okay password
  5 points: God tier Strong password

"""

UpperCase=any([1 if c in string.ascii_uppercase else 0 for c in password])
LowerCase=any([1 if c in string.ascii_lowercase else 0 for c in password])
SpecialChar=any([1 if c in string.punctuation else 0 for c in password])
Digits=any([1 if c in string.digits else 0 for c in password])
#Checks if any of the critera are present in the password entered by the user, if it is, add 1 to the array below

critera=[UpperCase,LowerCase,SpecialChar,Digits]
Score=0

if len(password)>=12:
  Score+=1
if sum(critera)>=1:
  Score+=1
if sum(critera)>=2:
  Score+=1
if sum(critera)>=3:
  Score+=1
if sum(critera)==4:
  Score+=1
# Checks the number of '1's in the array and sums them, if a password has all 4 critera, then you have 4 points in total
# Make your password at least 12 charachters long and you add one more point to make it 5

if len(password)>=12:
  print("Your password has a length of",length,"charachters! +1 point")
else:
    print("Your password has a length of",length,"charachters! + 0 points")

print("It has a combination of",sum(critera),"different charachter types")
print()

if Score == 1:
  print("Your password is weak! It has a score of",Score, "/ 5")
if Score == 2 or Score == 3 or Score==4:
  print("Your password is okay! It has a score of",Score, "/ 5")
if Score ==5:
  print("Your password is strong! It has a score of",Score, "/ 5")